//
//  NYCSConstant.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import Foundation

// Constant Variables that are used by the app
struct ConstantVars {
    static var username = ""
    static let baseUrl = "https://data.cityofnewyork.us/resource/"
    static let schoolsFile = "s3k6-pzi2.json"
    static let schoolSatFile = "f9bf-2cp4.json"
    static let sqlSelect = "$select="
    static let shoolsFilter = "school_name"
    static let schoolIdFilter = "dbn"
    static let schoolNameFilter = "school_name="
}
