//
//  NYCSDBManager.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import Foundation
import CoreData
import UIKit

class NYCSDBManager : NSObject {
    static var shared: NYCSDBManager{
        let instance = NYCSDBManager()
        return instance
    }
    
    var managedContext: NSManagedObjectContext!
    override init(){
        super.init()
        
        let appdelegate = UIApplication.shared.delegate as? AppDelegate
        self.managedContext = appdelegate?.persistentContainer.viewContext
    }
    
    // MARK: - Save Bookmark School
    // Save bookmark school using Schools CoreData Model
    func saveSchool(schoolDC: Schools) {
        let entity = NSEntityDescription.entity(forEntityName: "Schools", in: self.managedContext)
        let object = NSManagedObject.init(entity: entity!, insertInto: self.managedContext)
        
        object.setValue(schoolDC.schoolId, forKey: "schoolId")
        object.setValue(schoolDC.schoolName, forKey: "schoolName")
        
        do {
            try self.managedContext.save()
        } catch {
            print("Error in create new favorite: \(error)")
        }
    }
    
    // Save bookmark school using School Data Model
    func saveSchool(schoolDM: School) {
        let entity = NSEntityDescription.entity(forEntityName: "Schools", in: self.managedContext)
        let object = NSManagedObject.init(entity: entity!, insertInto: self.managedContext)
        
        object.setValue(schoolDM.schoolId, forKey: "schoolId")
        object.setValue(schoolDM.schoolName, forKey: "schoolName")
        
        do {
            try self.managedContext.save()
        } catch {
            print("Error in create new favorite: \(error)")
        }
    }
    
    // MARK: - Delete Bookmark School
    // Delete bookmark school using Schools CoreData Model
    func deleteSchool(school: Schools) {
        self.managedContext.delete(school)
        do{
            try self.managedContext.save()
        }
        catch{
            print("Delete data failed: \(error)")
        }
    }
    
    // Delete bookmark school using schoolId
    func deleteSchool(schoolId: String) {
        let request = NSFetchRequest<Schools>(entityName: "Schools")
        request.predicate = NSPredicate(format: "schoolId == %@", schoolId)
        
        do{
            guard let school = try self.managedContext.fetch(request).first else { return }
            self.managedContext.delete(school)
            try self.managedContext.save()
        }
        catch{
            print("Delete data failed: \(error)")
        }
    }
    
    // MARK: - Check Bookmark School in DB
    func checkSchoolExistInDB(schoolId: String) -> Bool {
        var bookmark: Schools?
        let request = NSFetchRequest<Schools>(entityName: "Schools")
        request.predicate = NSPredicate(format: "schoolId == %@", schoolId)
        
        do{
            bookmark = try self.managedContext.fetch(request).first
        } catch{
            print("Error in checking school exists: \(error)")
        }
        
        // Return approriate boolean if bookmark school exist in coredata
        return bookmark != nil ? true : false
    }
    
    // MARK: - Get Bookmark Schools
    func getBookmarkSchools() -> [Schools]{
        let request = NSFetchRequest<Schools>(entityName: "Schools")
        
        do{
            let result = try self.managedContext.fetch(request)
            return result
        } catch{
            print("Error in fetching Favorite Photos: \(error)")
        }
        
        return [Schools]()
    }
}
