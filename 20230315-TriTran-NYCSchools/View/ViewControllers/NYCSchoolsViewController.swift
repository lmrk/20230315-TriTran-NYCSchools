//
//  NYCSchoolsViewController.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import UIKit

class NYCSchoolsViewController: UIViewController {
    
    // Storyboard links properties
    @IBOutlet weak var schoolSearchBar: UISearchBar!
    @IBOutlet weak var schoolsTableView: UITableView!
    
    // Properties declaration
    var nycsViewModel = {
        NYCSViewModel()
    }()
    var schoolsData: [School] = []
    var filterSchools: [School] = []
    var selectedSchool: School?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set properties for tableview
        schoolsTableView.dataSource = self
        schoolsTableView.delegate = self
        schoolsTableView.estimatedRowHeight = 80
        
        // Set properties for search bar
        schoolSearchBar.delegate = self
        
        // Set properties for NYCS view model
        // get list of school from API
        nycsViewModel.delegate = self
        nycsViewModel.fetchSchoolNames()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        schoolsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewSchoolSatScore" {
            let destinationVC = segue.destination as! SchoolSatScorelViewController
            destinationVC.school = selectedSchool
        }
    }
}

// MARK: - UITableView Delegate
extension NYCSchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterSchools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "schoolTableViewCell") as? SchoolTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(school: filterSchools[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedSchool = filterSchools[indexPath.row]
        self.performSegue(withIdentifier: "viewSchoolSatScore", sender: self)
    }
    // Dismiss keyboard when scroll
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        schoolSearchBar.endEditing(true)
    }
}

// MARK: - NYCSViewModel Delegate
extension NYCSchoolsViewController: NYCSViewDelegation {
    func doneGettingSchoolNames(schools: [School]) {
        self.schoolsData = schools
        self.filterSchools = schools
        DispatchQueue.main.async {[weak self] in
            self?.schoolsTableView.reloadData()
        }
    }
}

// MARK: - UISearchBar Delegate
extension NYCSchoolsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterSchools = []
        
        // Load full data if searchbar text is empty
        if searchText.isEmpty {
            filterSchools = schoolsData
        }
        
        // Get list of all school names that contains
        // searchbar text
        for school in schoolsData {
            if school.schoolName.uppercased().contains(searchText.uppercased()) {
                filterSchools.append(school)
            }
        }
        
        self.schoolsTableView.reloadData()
    }
    
    // Dismiss keyboard after press search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        schoolSearchBar.endEditing(true)
    }
}
