//
//  SchoolSatScoreViewController.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import UIKit

class SchoolSatScorelViewController: UIViewController {

    // Storyboard links properties
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var mathScore: UILabel!
    @IBOutlet weak var readingScore: UILabel!
    @IBOutlet weak var writingScore: UILabel!
    
    // Properties declaration
    var school: School?
    var satScoreViewModel = {
        NYCSViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set properties for NYCS View Model
        // fetch the sat score for the school
        satScoreViewModel.delegate = self
        if let schoolId = school?.schoolId {
            satScoreViewModel.fetchSchoolSatScore(schoolId: schoolId)
        }
    }
}

// MARK: - NYCSViewModel Delegate
extension SchoolSatScorelViewController: NYCSViewDelegation {
    func doneGettingSchoolSatCore(schoolSatScore: SchoolSatScore) {
        
        // Set school name and sat score for reading, writing, math
        DispatchQueue.main.async {[weak self] in
            self?.schoolName.text = schoolSatScore.schoolName.isEmpty ?
                self?.school?.schoolName.uppercased() :
                schoolSatScore.schoolName
            self?.mathScore.text = schoolSatScore.mathScore
            self?.readingScore.text = schoolSatScore.readingScore
            self?.writingScore.text = schoolSatScore.writingScore
        }
    }
}
