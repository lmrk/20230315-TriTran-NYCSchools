//
//  SchoolBookmarkTableViewCell.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import UIKit

class SchoolBookmarkTableViewCell: UITableViewCell {

    // Storyboard links properties
    @IBOutlet weak var schoolName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // Helper function to set up the cell with school name
    func configure(school: Schools) {
        schoolName.text = school.schoolName
    }
}
