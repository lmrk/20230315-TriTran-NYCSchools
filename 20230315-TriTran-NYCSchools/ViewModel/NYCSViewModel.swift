//
//  NYCSViewModel.swift
//  20230315-TriTran-NYCSchools
//
//  Created by Tri Tran on 3/15/23.
//

import Foundation

// MARK: NYCSViewModel Delegate
// Create optional delegates so it can be use by multiple ViewControllers
protocol NYCSViewDelegation: AnyObject {
    func doneGettingSchoolNames(schools: [School])
    func doneGettingSchoolSatCore(schoolSatScore: SchoolSatScore)
}
extension NYCSViewDelegation {
    func doneGettingSchoolNames(schools: [School]){
    }
    
    func doneGettingSchoolSatCore(schoolSatScore: SchoolSatScore) {
    }
}

// MARK: - NYCSViewModel Class
class NYCSViewModel {
    weak var delegate: NYCSViewDelegation?
    
    // Function to call NYCSAPIManager and get the school name from API
    func fetchSchoolNames() {
        NYCSAPIManager.shared.getSchoolNames(completionHandler: {[weak self] (schools, error) in
            // Handle the errors after getting schools data
            if error != nil {
                print(error!)
            }
            
            // Pass data back to ViewController that call this method
            // and implement the doneGettingSchoolNames delegate
            else {
                guard let schoolList = schools else {return}
                self?.delegate?.doneGettingSchoolNames(schools: schoolList)
            }
        })
    }
    
    func fetchSchoolSatScore(schoolId: String) {
        NYCSAPIManager.shared.getSchoolSatScore(schoolId: schoolId, completionHandler: {[weak self] (schoolSatScoreData, error) in
            
            // Handle the errors after getting school sat score data
            if error != nil {
                print(error!)
            }
            
            // Pass data back to ViewController that call this method
            // and implement the doneGettingSchoolNames delegate
            else {
                // Set sat score data if there is values, otherwise leaving
                // empty and N/A
                let schoolSatScore = schoolSatScoreData?.first ?? SchoolSatScore(schoolName: "", readingScore: "N/A", mathScore: "N/A", writingScore: "N/A")
                self?.delegate?.doneGettingSchoolSatCore(schoolSatScore: schoolSatScore)
            }
        })
    }
}
